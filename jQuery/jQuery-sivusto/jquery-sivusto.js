$(document).ready(function(){
	
	$("#sivupalkki h3").click(function(event) {
		var navi = $(this).parent();
		var a = navi.attr("class");
		navi.addClass("auki");
		if(a == "auki") {
			navi.removeClass("auki");
			navi.addClass("kiinni");
		}
		else if(a == "kiinni") {
			navi.removeClass("kiinni");
			navi.addClass("auki");
		}
    });
	
	$("#sisalto nav li").click(function(event) {
		$("#sisalto nav li").removeClass("valittu");
		$("#tab1, #tab2, #tab3").removeClass("valittu");
		$(this).addClass("valittu");
		
		var tabnumero = $(this).attr("data-tab");
		$("#tab" + tabnumero).addClass("valittu");
	});
	
	//lomakkeen käsittely
	
	$("#salasanakentta1").focus(function() {
        $(this).parent().children(".ohje").show();
    });

    $("#salasanakentta1").blur(function() {
        $(this).parent().children(".ohje").hide();
	});
	
	$("#hyvaksykayttoehdot").click(function() {
		if($("#hyvaksykayttoehdot")[0].checked) {
			$("#nappi").removeAttr("disabled");
		}
		else {
			$("#nappi").attr("disabled", true);
		}
	});
	
	$("#salasanakentta1").keyup(function() {
		if($(this).val().length < 8) {
			$(this).css("background-color", "pink");
		}
		else {
			$(this).css("background-color", "lightgreen");
		}
		
		if($(this).val() == $("#salasanakentta2").val()) {
			$("#salasanakentta2").css("background-color", "lightgreen");
		}
		else {
			$("#salasanakentta2").css("background-color", "pink");
		}
	});
	
	$("#salasanakentta2").keyup(function() {
		if($(this).val() == $("#salasanakentta1").val()) {
			$(this).css("background-color", "lightgreen");
		}
		else {
			$(this).css("background-color", "pink");
		}
	});
	
	$("#rekisteroityminen").submit(function() {
		if($("#nimimerkkikentta").val() == "") {
			$("#nimimerkkikentta").parent().children(".rlabel").css("color", "red");
		}
		else {
			$("#nimimerkkikentta").parent().children(".rlabel").css("color", "black");
		}
		
		if($("#sahkopostikentta").val() == "") {
			$("#sahkopostikentta").parent().children(".rlabel").css("color", "red");
		}
		else {
			$("#sahkopostikentta").parent().children(".rlabel").css("color", "black");
		}
		
		if($("#salasanakentta1").val() == "") {
			$("#salasanakentta1").parent().children(".rlabel").css("color", "red");
		}
		else {
			$("#salasanakentta1").parent().children(".rlabel").css("color", "black");
		}
		
		if($("#salasanakentta2").val() == "") {
			$("#salasanakentta2").parent().children(".rlabel").css("color", "red");
		}
		else {
			$("#salasanakentta2").parent().children(".rlabel").css("color", "black");
		}
		
		if(($("#nimimerkkikentta").val() == "") || ($("#sahkopostikentta").val() == "") || ($("#salasanakentta1").val() == "") || ($("#salasanakentta2").val() == "") ) {
			return false;
		}
	});
	
	//laskentataulukko
	
	$("#lisaarivi").click(function() {
		$("#laskentataulukko").append("<tr><td></td><td></td><td></td><td></td><td></td><td class=\"poisto\"><button>Poista</button></td></tr>");
	});
	
	$("#laskentataulukko").on("click", ".poisto", function(){
		$(this).parent().remove();
	});
	
	$("#laskentataulukko td").dblclick(function() {
		$("#laskentataulukko td").removeAttr("contenteditable");
		$(this).attr("contenteditable", true);
	});
	
	$(document).keypress(function(e) {
		if(e.which == 13) {
			$("#laskentataulukko td").removeAttr("contenteditable");
			$(this).attr("contenteditable", true);
		}
	});
});