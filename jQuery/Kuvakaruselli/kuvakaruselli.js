﻿$(document).ready(function(){
    var nykyinen = 1;
    var ajastin;
    ajastin = setInterval(seuraavaKuva, 5000);
	
$("#tab1").click(function() {
    $("#valilehdet span").removeClass("valittu");
    $(".kuva").removeClass("valittu");
    $("#tab1").addClass("valittu");
    $("#kuva1").addClass("valittu");
    nykyinen = 1;
    clearInterval(ajastin);
    ajastin = setInterval(seuraavaKuva, 5000);
});

$("#tab2").click(function() {
    $("#valilehdet span").removeClass("valittu");
    $(".kuva").removeClass("valittu");
    $("#tab2").addClass("valittu");
    $("#kuva2").addClass("valittu");
    nykyinen = 2;
    clearInterval(ajastin);
    ajastin = setInterval(seuraavaKuva, 5000);
});

$("#tab3").click(function() {
    $("#valilehdet span").removeClass("valittu");
    $(".kuva").removeClass("valittu");
    $("#tab3").addClass("valittu");
    $("#kuva3").addClass("valittu");
    nykyinen = 3;
    clearInterval(ajastin);
    ajastin = setInterval(seuraavaKuva, 5000);
});

$("#tab4").click(function() {
    $("#valilehdet span").removeClass("valittu");
    $(".kuva").removeClass("valittu");
    $("#tab4").addClass("valittu");
    $("#kuva4").addClass("valittu");
    nykyinen = 4;
    clearInterval(ajastin);
    ajastin = setInterval(seuraavaKuva, 5000);
});



function seuraavaKuva() {
	nykyinen++;
	if(nykyinen == 5)
		nykyinen = 1;
	$("#valilehdet span").removeClass("valittu");
	$(".kuva").removeClass("valittu");
	$("#tab" + nykyinen).addClass("valittu");
	$("#kuva" + nykyinen).addClass("valittu");
}

} );
